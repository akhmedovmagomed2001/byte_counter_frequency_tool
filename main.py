import matplotlib.pyplot as plt
from decimal import *
import sys

def readCharactersFromFile(filename):
  characters = []

  with open(filename, "rb") as f:
    byte = f.read(1)
    while byte:
      characterNumber = int.from_bytes(byte, "big")
      characters.append(characterNumber)
      byte = f.read(1)

  return characters


def createEmptyASCIIDict():
  ascIIdict = {}
  for x in range(256):
    ascIIdict[x] = 0
  
  return ascIIdict


def calculateCharactersCount(charactersList):
  charactersCount = createEmptyASCIIDict();

  for character in charactersList:
    characterValue = charactersCount[character]
    characterValue += 1
    charactersCount[character] = characterValue

  return charactersCount


def calculateCharactersFrequency(charactersList):
  getcontext().prec = 6

  frequencyDict = {}

  charactersCountDict = calculateCharactersCount(charactersList)
  charactersCount = len(charactersList)

  uniqueCharacters = list(charactersCountDict.keys())

  for character in uniqueCharacters:
    characterCount = charactersCountDict[character]
    frequency = Decimal(characterCount) / Decimal(charactersCount)
    frequencyRounded = round(frequency, 4)
    frequencyDict[character] = float(frequencyRounded)

  return frequencyDict


def createBarChart(charactersCount, plotTitle, outputFile):
  xList = range(len(charactersCount))
  yList = []

  for i in list(charactersCount.keys()):
    yList.append(charactersCount[i])

  f = plt.figure()
  f.set_figwidth(35)
  f.set_figheight(10)

  plt.bar(xList, yList, width = 0.3, color = ['red'])
  plt.xlabel("Byte")
  plt.ylabel("Count")
  plt.title(plotTitle)

  plt.savefig(outputFile)


def main(argv):
    if len(argv) != 2:
        print("Script requires 1 argument: <inputFilename>")
        sys.exit(2)

    inputFilename = argv[1]
    filenameWithoutExtention = inputFilename.split(".")[0]
    
    charactersList = readCharactersFromFile(inputFilename)
    
    charactersCount = calculateCharactersCount(charactersList)
    countChartTitle = "Character count in '{}'".format(inputFilename)
    countChartOutputFilename = "{}_countChart.pdf".format(filenameWithoutExtention)
    createBarChart(charactersCount, countChartTitle, countChartOutputFilename)
    
    charactersFrequancy = calculateCharactersFrequency(charactersList)
    frequencyChartTitle = "Character frequency in '{}'".format(inputFilename)
    frequencyChartOutputFilename = "{}_frequencyChart.pdf".format(filenameWithoutExtention)
    createBarChart(charactersFrequancy, frequencyChartTitle, frequencyChartOutputFilename)

if __name__ == "__main__":
    main(sys.argv)

